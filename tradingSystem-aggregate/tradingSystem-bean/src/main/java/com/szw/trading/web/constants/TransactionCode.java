package com.szw.trading.web.constants;

/**
 * 
 * <P>交易组件常量</P>
 * 
 * @author 苏镇威 2018年8月14日 上午9:30:07
 */
public interface TransactionCode {

	final String CURRENY_CNY = "CNY";

	final String S_0000 = "0000";
	final String S_0000_M = "SUCCESS";

	final String C_0003 = "0003";
	final String C_0003_M = "交易关闭";

	final String P_0007 = "0007";
	final String P_0007_M = "订单未支付";

	final String P_0008 = "0008";
	final String P_0008_M = "交易处理中";

	final String F_0009 = "0009";
	final String F_0009_M = "交易失败";

	final String E_9001 = "9001";
	final String E_9001_M = "银行前置通讯异常";

	final String E_9999 = "9999";
	final String E_9999_M = "未知错误";

	final String E_1000 = "1000";
	final String E_1000_M = "账户已存在";

	final String E_1001 = "1001";
	final String E_1001_M = "该微信注册二维码不存在";

	final String E_1002 = "1002";
	final String E_1002_M = "缺少微信注册二维码";

	final String E_1003 = "1003";
	final String E_1003_M = "系统已存在该账户";

	final String E_1004 = "1004";
	final String E_1004_M = "该二维码已被他人注册使用";

	final String E_1005 = "1005";
	final String E_1005_M = "支行不存在";

	final String E_1006 = "1006";
	final String E_1006_M = "该商户名称已存在";

	final String E_1007 = "1007";
	final String E_1007_M = "地区编码不存在";

	final String E_1010 = "1010";
	final String E_1010_M = "无此增值服务";

	final String E_1011 = "1011";
	final String E_1011_M = "客户增值服务已存在";

	final String E_1012 = "1012";
	final String E_1012_M = "代理商实时结算分润配置不存在";

	final String E_1013 = "1013";
	final String E_1013_M = "商户增值产品费率输入不正确";

	final String E_1014 = "1014";
	final String E_1014_M = "商户费率低于阈值";

	// =================交易风控 Start=============================
	final String E_6000 = "6000";
	final String E_6000_M = "风控检查未通过";

	final String E_6002 = "6002";
	final String E_6002_M = "交易金额超过扫码支付单笔限额，请尝试降低交易额完成支付";

	final String E_6003 = "6003";
	final String E_6003_M = "尚未开通相应的业务服务";

	final String E_6004 = "6004";
	final String E_6004_M = "没有配置商户手续费，不可交易";

	final String E_6005 = "6005";
	final String E_6005_M = "超过当日该商户的扫码支付限额，请尝试通过其他方式完成支付";

	final String E_6006 = "6006";
	final String E_6006_M = "交易金额未达到最低限额，不可交易";

	final String E_6007 = "6007";
	final String E_6007_M = "规定时间内，您的交易次数已达上限，请勿频繁操作！";

	final String E_6008 = "6008";
	final String E_6008_M = "退款金额不能大于订单金额";

	final String E_6009 = "6009";
	final String E_6009_M = "历史总退款金额不能大于订单金额";

	final String E_6010 = "6010";
	final String E_6010_M = "该订单还有未完成的退款订单，请等待退款完成之后再申请退款";

	// =================交易风控 End=============================

	final String E_7001 = "7001";
	final String E_7001_M = "没有找到收款方嘉捷通账户";

	final String E_7002 = "7002";
	final String E_7002_M = "付款方没有登录";

	final String E_7003 = "7003";
	final String E_7003_M = "平台编码为空";

	final String E_7004 = "7004";
	final String E_7004_M = "交易方式为空";

	final String E_7005 = "7005";
	final String E_7005_M = "交易分类为空";

	final String E_7006 = "7006";
	final String E_7006_M = "订单号为空";

	final String E_7007 = "7007";
	final String E_7007_M = "消费名称为空";

	final String E_7008 = "7008";
	final String E_7008_M = "付款金额为空";

	final String E_7009 = "7009";
	final String E_7009_M = "平台分润金额为空";

	final String E_7010 = "7010";
	final String E_7010_M = "同步通知页面地址为空";

	final String E_7011 = "7011";
	final String E_7011_M = "数字签名校验失败";

	final String E_7012 = "7012";
	final String E_7012_M = "没有找到平台";

	final String E_7013 = "7013";
	final String E_7013_M = "无效订单类型";

	final String E_7014 = "7014";
	final String E_7014_M = "无效交易号";

	final String E_7015 = "7015";
	final String E_7015_M = "无效合并付款号";

	final String E_7016 = "7016";
	final String E_7016_M = "无效支付方式";

	final String E_7017 = "7017";
	final String E_7017_M = "支付金额与交易金额不相等";

	final String E_7018 = "7018";
	final String E_7018_M = "合并支付交易方式必须一致";

	final String E_7019 = "7019";
	final String E_7019_M = "账户余额不足";

	final String E_7020 = "7020";
	final String E_7020_M = "异步通知地址为空";

	final String E_7021 = "7021";
	final String E_7021_M = "订单已经支付过了";

	final String E_7022 = "7022";
	final String E_7022_M = "订单状态错误";

	final String E_7023 = "7023";
	final String E_7023_M = "金额格式错误";

	final String E_7024 = "7024";
	final String E_7024_M = "订单已存在";

	final String E_7025 = "7025";
	final String E_7025_M = "无效订单过期时间";

	final String E_7026 = "7026";
	final String E_7026_M = "订单未支付";

	final String E_7030 = "7030";
	final String E_7030_M = "业务类型为空";

	final String E_7031 = "7031";
	final String E_7031_M = "业务对象类型为空";

	final String E_7032 = "7032";
	final String E_7032_M = "付款人账号为空";

	final String E_7033 = "7033";
	final String E_7033_M = "付款人名称为空";

	final String E_7034 = "7034";
	final String E_7034_M = "卡折标志为空";

	final String E_7035 = "7035";
	final String E_7035_M = "付款行名称为空";

	final String E_7036 = "7036";
	final String E_7036_M = "付款省份编码为空";

	final String E_7037 = "7037";
	final String E_7037_M = "用途说明为空";

	final String E_7038 = "7038";
	final String E_7038_M = "该卡未认证";

	final String E_7039 = "7039";
	final String E_7039_M = "生成订单异常";

	final String E_7040 = "7040";
	final String E_7040_M = "付款人账户行别为空";

	final String E_7041 = "7041";
	final String E_7041_M = "付款人账户开户行名称为空";

	final String E_7042 = "7042";
	final String E_7042_M = "客户流水摘要为空";

	final String E_7043 = "7043";
	final String E_7043_M = "证件号码为空";

	final String E_7044 = "7044";
	final String E_7044_M = "用途说明为空";

	final String E_7045 = "7045";
	final String E_7045_M = "无效订单";

	final String E_7046 = "7046";
	final String E_7046_M = "订单更新异常";

	final String E_7047 = "7047";
	final String E_7047_M = "个人信息为空";

	final String E_7048 = "7048";
	final String E_7048_M = "银行卡不正确";

	final String E_7049 = "7049";
	final String E_7049_M = "签名为空";

	final String E_7050 = "7050";
	final String E_7050_M = "接口业务类型为空";

	final String E_7051 = "7051";
	final String E_7051_M = "接口业务类型不正确";

	final String E_7052 = "7052";
	final String E_7052_M = "参数编码字符集为空";

	final String E_7053 = "7053";
	final String E_7053_M = "参数编码字符集不正确";

	final String E_7054 = "7054";
	final String E_7054_M = "接口版本为空";

	final String E_7055 = "7055";
	final String E_7055_M = "接口版本不正确";

	final String E_7056 = "7056";
	final String E_7056_M = "签名方式为空";

	final String E_7057 = "7057";
	final String E_7057_M = "签名方式不正确";

	final String E_7058 = "7058";
	final String E_7058_M = "银行卡号为空";

	final String E_7059 = "7059";
	final String E_7059_M = "消费者真实姓名为空";

	final String E_7060 = "7060";
	final String E_7060_M = "消费者身份证为空";

	final String E_7061 = "7061";
	final String E_7061_M = "查询类别不正确";

	final String E_7062 = "7062";
	final String E_7062_M = "查询类别为空";

	final String E_7063 = "7063";
	final String E_7063_M = "商家密钥为空";

	final String E_7064 = "7064";
	final String E_7064_M = "验签失败";

	final String E_7065 = "7065";
	final String E_7065_M = "证书过期或已失效";

	final String E_7066 = "7066";
	final String E_7066_M = "银行代码为空";

	final String E_7067 = "7067";
	final String E_7067_M = "订单信息不存在";

	final String E_7068 = "7068";
	final String E_7068_M = "银行订单不存在";

	final String E_7099 = "7099";
	final String E_7099_M = "加密数据为空";

	final String E_7100 = "7100";
	final String E_7100_M = "参数不足或参数错误";

	final String E_7101 = "7101";
	final String E_7101_M = "用户禁止授权";

	final String E_7102 = "7102";
	final String E_7102_M = "手机验证码输入不正确";

	final String E_7103 = "7103";
	final String E_7103_M = "手机验证码已失效";

	final String E_7104 = "7104";
	final String E_7104_M = "非法请求";

	final String E_7110 = "7110";
	final String E_7110_M = "没有支付通道";

}
