package com.szw.trading.web.bean;

import java.math.BigDecimal;

public class RechargeAccountRequest extends BaseRequest {

	private static final long serialVersionUID = 8459577366134897674L;
	private BigDecimal amount;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
