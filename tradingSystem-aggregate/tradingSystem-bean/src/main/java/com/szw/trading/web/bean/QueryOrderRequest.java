package com.szw.trading.web.bean;

public class QueryOrderRequest extends SearchRequest {

	private static final long serialVersionUID = -1872836303957756971L;

	private String orderType;
	private String orderStatus;

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
}
