package com.szw.trading.web.bean;

import org.hibernate.validator.constraints.NotEmpty;


public class CancelOrderRequest extends BaseRequest {
	private static final long serialVersionUID = -2890746058861519056L;
	/** 用户Id */
	private String loginId;
	@NotEmpty(message = "必须输入")
	private String cancelOrderNo;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCancelOrderNo() {
		return cancelOrderNo;
	}

	public void setCancelOrderNo(String cancelOrderNo) {
		this.cancelOrderNo = cancelOrderNo;
	}

}
