package com.szw.trading.web.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.PasswordTransformerResponse;
import com.szw.trading.web.bean.Response;
import com.szw.trading.web.service.PublicService;


@Controller
public class PasswordTransformerController extends CommonController<String> {

	@Autowired
	private PublicService publicService;

	@RequestMapping("/api/public/passwordTransformer/{password}")
	@ResponseBody
	public PasswordTransformerResponse passwordTransformer(@PathVariable String password) {
		return this.publicService.passwordTransformer(password);
	}

	@Override
	protected Response bizProcess(Principal principal, String request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
