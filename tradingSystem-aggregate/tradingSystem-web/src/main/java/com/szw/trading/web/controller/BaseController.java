package com.szw.trading.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;


public class BaseController {
	public final Logger log = LoggerFactory.getLogger(getClass());

	protected static final String ENCODING = "UTF-8";

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) {
		// 自动转换日期类型的字段格式
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
	}

	/**
	 * 通过servlet输出controller返回结果
	 * 
	 * @param result
	 * @param response
	 */
	protected void writeResult(String result, HttpServletResponse response) {
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("utf-8");
		try (PrintWriter out = response.getWriter()) {
			out.write(result);
			out.flush();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 通过servlet输出controller返回结果(表单校验控件用)
	 * 
	 * @param code y 验证成功 n 验证失败
	 * @param msg 提示信息
	 */
	protected void writeValidformResult(String code, String msg, HttpServletResponse response) {
		StringBuffer jsonStr = new StringBuffer("{\"info\":\"");
		jsonStr.append(null == msg ? "" : msg.trim());
		jsonStr.append("\",\"status\":\"");
		jsonStr.append(null == code ? "n" : code.trim());
		jsonStr.append("\"}");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("utf-8");
		try (PrintWriter out = response.getWriter()) {
			out.write(jsonStr.toString());
			out.flush();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 获取异常的栈信息
	 * 
	 * @param e
	 * @return
	 */
	protected String getStackMsg(Exception e) {
		StringBuffer msg = new StringBuffer("");
		if (e != null) {
			String message = e.toString();
			msg.append(message + "\n");
			int length = e.getStackTrace().length;
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					msg.append("\t" + e.getStackTrace()[i] + "\n");
				}
			} else {
				msg.append(message);
			}
		}
		return msg.toString();
	}

	protected String getCurrentDateforLog() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return format.format(new Date());
	}

	protected String getServerPath(HttpServletRequest request) {
		String basePath = "";
		String serverName = request.getServerName();
		if (serverName != null && serverName.contains("willso.cn")) {
			basePath = "http://" + serverName + request.getContextPath();
		} else {
			basePath = request.getScheme() + "://" + serverName + ":" + request.getServerPort() + request.getContextPath();
		}
		return basePath;
	}

	// public String getValidString(BindingResult bindingResult) {
	// return bindingResult.getFieldError().getField() +
	// bindingResult.getFieldError().getDefaultMessage();
	// }
}
