package com.szw.trading.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.CreateOrderRequest;
import com.szw.trading.web.bean.Response;
import com.szw.trading.web.service.CustomerService;
import com.szw.util.LogMapUtils;


@Controller
public class CreateOrderController extends CommonController<CreateOrderRequest> {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/api/customer/createOrder")
	@ResponseBody
	public Response createOrder(Principal principal, @Valid @RequestBody CreateOrderRequest request, BindingResult bindingResult,
			HttpServletRequest httpRequest) {
		log.info("[创建订单] begin: the order = {}, sessionId = {}", LogMapUtils.toLogMap(request), httpRequest.getSession().getId());
		// 获得业务处理响应结果
		Response response = super.getResponse(principal, request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(Principal principal, CreateOrderRequest request) throws Exception {
		return customerService.createOrder(principal, request);
	}

}
