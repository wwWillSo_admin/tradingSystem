package com.szw.trading.web.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.Response;
import com.szw.trading.web.bean.UserRegisterRequest;
import com.szw.trading.web.service.PublicService;
import com.szw.util.LogMapUtils;


@Controller
public class UserRegisterController extends CommonController<UserRegisterRequest> {

	@Autowired
	private PublicService publicService;

	@RequestMapping("/api/public/userRegister")
	@ResponseBody
	public Response userRegister(Principal principal, @Valid @RequestBody UserRegisterRequest request, BindingResult bindingResult) {
		log.info("[用户注册] begin: the request = {}", LogMapUtils.toLogMap(request));
		// 获得业务处理响应结果
		Response response = super.getResponse(principal, request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(Principal principal, UserRegisterRequest request) throws Exception {
		return this.publicService.userRegister(request);
	}

}
