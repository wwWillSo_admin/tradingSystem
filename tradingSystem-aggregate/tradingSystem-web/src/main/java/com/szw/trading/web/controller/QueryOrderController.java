package com.szw.trading.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.QueryOrderRequest;
import com.szw.trading.web.bean.Response;
import com.szw.trading.web.service.CustomerService;
import com.szw.util.LogMapUtils;


@Controller
public class QueryOrderController extends CommonController<QueryOrderRequest> {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/api/customer/queryOrder")
	@ResponseBody
	public Response queryOrder(Principal principal, @RequestBody QueryOrderRequest request, HttpServletRequest hsRequest, BindingResult bindingResult) {
		// log.info("查询订单接口sessionID = " + hsRequest.getSession().getId());
		log.info("[查询订单] begin: the order = {}, sessionId = {}", LogMapUtils.toLogMap(request), hsRequest.getSession().getId());
		// 获得业务处理响应结果
		Response response = super.getResponse(principal, request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(Principal principal, QueryOrderRequest request) throws Exception {
		return customerService.queryOrder(principal, request);
	}

}
