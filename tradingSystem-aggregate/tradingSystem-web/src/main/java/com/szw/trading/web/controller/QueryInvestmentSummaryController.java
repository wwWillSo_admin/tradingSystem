package com.szw.trading.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.Response;
import com.szw.trading.web.bean.SearchRequest;
import com.szw.trading.web.service.CustomerService;
import com.szw.util.LogMapUtils;


@Controller
public class QueryInvestmentSummaryController extends CommonController<SearchRequest> {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/api/customer/queryInvestmentSummary")
	@ResponseBody
	public Response queryInvestmentSummary(Principal principal, @RequestBody SearchRequest request, BindingResult bindingResult,
			HttpServletRequest httpRequest) {
		log.info("[查询持仓] begin: the order = {}, sessionId = {}", LogMapUtils.toLogMap(request), httpRequest.getSession().getId());
		// 获得业务处理响应结果
		Response response = super.getResponse(principal, request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(Principal principal, SearchRequest request) throws Exception {
		return customerService.queryInvestmentSummary(principal, request);
	}

}
