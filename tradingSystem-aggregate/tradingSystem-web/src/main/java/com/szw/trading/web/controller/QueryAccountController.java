package com.szw.trading.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.web.bean.Response;
import com.szw.trading.web.service.CustomerService;
import com.szw.util.LogMapUtils;


@Controller
public class QueryAccountController extends CommonController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/api/customer/queryAccount")
	@ResponseBody
	public Response queryAccount(Principal principal, HttpServletRequest httpRequest) {
		log.info("[查询账户] begin: the principal = {}, sessionId = {}", LogMapUtils.toLogMap(principal), httpRequest.getSession().getId());
		// 获得业务处理响应结果
		return customerService.queryAccount(principal);
	}

	@Override
	protected Response bizProcess(Principal principal, Object request) throws Exception {
		return null;
	}

}
