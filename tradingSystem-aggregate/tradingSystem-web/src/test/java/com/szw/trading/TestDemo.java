package com.szw.trading;

/**
 * 1、饿汉模式（静态常量）【可用】
 * 
 * 优点：写法简单，避免了线程同步问题
 * 
 * 缺点：没有达到lazy-loading的效果，如果这个实例一直没被使用，那就是浪费。
 * 
 * @author 苏镇威 2020年4月7日 下午1:27:18
 */
class Singleton1 {
	private final static Singleton1 INSTANCE = new Singleton1();

	private Singleton1() {

	}

	public static Singleton1 getInstance() {
		return INSTANCE;
	}
}

/**
 * 2、饿汉式（静态代码块）【可用】
 * 
 * 优缺点与上面的一致
 * 
 * @author 苏镇威 2020年4月7日 下午1:31:58
 */
class Singleton2 {
	private static Singleton2 INSTANCE;
	static {
		INSTANCE = new Singleton2();
	}

	private Singleton2() {

	}

	public static Singleton2 getInstance() {
		return INSTANCE;
	}
}

/**
 * 3、懒汉式（线程不安全）【不可用】
 * 
 * 只能在单线程下使用，如果是多线程，会产生多个实例。
 * 
 * @author 苏镇威 2020年4月7日 下午1:34:49
 */
class Singleton3 {
	private static Singleton3 INSTANCE;

	private Singleton3() {

	}

	public static Singleton3 getInstance() {
		if (null == INSTANCE) {
			INSTANCE = new Singleton3();
		}
		return INSTANCE;
	}
}

/**
 * 4、懒汉式（线程安全，同步方法）【不推荐用】
 * 
 * 解决上面的线程不安全问题。
 * 
 * 缺点：效率太低，每个线程进来都要进行同步，实际上这个方法只需要实例化代码一次就够了，没必要每次都同步进来判断一遍
 * 
 * @author 苏镇威 2020年4月7日 下午1:37:18
 */
class Singleton4 {
	private static Singleton4 INSTANCE;

	private Singleton4() {

	}

	public static synchronized Singleton4 getInstance() {
		if (null == INSTANCE) {
			INSTANCE = new Singleton4();
		}
		return INSTANCE;
	}
}

/**
 * 5、懒汉式（线程安全，同步代码块）【不可用】
 * 
 * 多线程还是会产生多个实例。
 * 
 * @author 苏镇威 2020年4月7日 下午1:39:51
 */
class Singleton5 {
	private static Singleton5 INSTANCE;

	private Singleton5() {

	}

	public static Singleton5 getInstance() {
		if (null == INSTANCE) {
			synchronized (Singleton5.class) {
				INSTANCE = new Singleton5();
			}
		}
		return INSTANCE;
	}
}

/**
 * 6、双重检查【推荐用】
 * 
 * 优点：线程安全，延迟加载，效率较高
 * 
 * @author 苏镇威 2020年4月7日 下午1:41:28
 */
class Singleton6 {
	private static Singleton6 INSTANCE;

	private Singleton6() {

	}

	public static Singleton6 getInstance() {
		if (null == INSTANCE) {
			synchronized (Singleton6.class) {
				if (null == INSTANCE) {
					INSTANCE = new Singleton6();
				}
			}
		}
		return INSTANCE;
	}
}

/**
 * 7、静态内部类【推荐用】
 * 
 * 线程安全，延迟加载，效率较高
 * 
 * @author 苏镇威 2020年4月7日 下午1:43:50
 */
class Singleton7 {
	private Singleton7() {

	}

	private static class SingletonInstance {
		private static final Singleton7 INSTANCE = new Singleton7();
	}

	public static Singleton7 getInstance() {
		return SingletonInstance.INSTANCE;
	}
}

/**
 * 8、枚举【推荐用】
 * 
 * 线程安全，延迟加载，效率高，防止反序列化重新创建新的对象。JDK1.5后出现。
 * 
 * @author 苏镇威 2020年4月7日 下午1:48:05
 */
enum Singleton8 {
	INSTANCE;
	public void doSth() {
		System.out.println("asd");
	}
}

public class TestDemo {
	public static void main(String[] args) {
		Singleton8.INSTANCE.doSth();
	}
}
