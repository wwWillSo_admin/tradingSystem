package com.szw.trading.order.controller;

import org.springframework.validation.BindingResult;

import com.szw.trading.web.bean.Response;
import com.szw.trading.web.constants.TransactionCode;


public abstract class CommonController<T> extends BaseController {

	/**
	 * 
	 * <p>获得客户端响应数据</p>
	 * 
	 * @param principal
	 * @param request
	 * @param bindingResult
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:00:56
	 */
	protected Response getResponse(T request, BindingResult bindingResult) {
		Response response = getValidResponse(request, bindingResult);
		if (response == null) {
			response = getBizResponse(request);
		}
		log.info("处理结果的报文：retcode = {}, retmsg = {}", response.get_ReturnCode(), response.get_ReturnMsg());
		return response;
	}

	/**
	 * 
	 * <p>获得验证客户端请求参数响应数据</p>
	 * 
	 * @param request
	 * @param bindingResult
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:01:10
	 */
	private Response getValidResponse(T request, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			log.warn("[{}]{}", bindingResult.getFieldError().getField(), bindingResult.getFieldError().getDefaultMessage());
			return getErrResponse(String.format("[%s]%s", bindingResult.getFieldError().getField(), bindingResult.getFieldError().getDefaultMessage()));
		}
		return getValidResponse(request);
	}

	/**
	 * 
	 * <p>各个子类如有特殊逻辑验证可重写此方法</p>
	 * 
	 * @param request
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:01:21
	 */
	protected Response getValidResponse(T request) {
		return null;
	}

	/**
	 * 
	 * <p>获得请求失败响应数据</p>
	 * 
	 * @param errMsg
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:01:33
	 */
	protected Response getErrResponse(String errMsg) {
		return getErrResponse(TransactionCode.E_7100, errMsg);
	}

	/**
	 * 
	 * <p>获得请求失败响应数据</p>
	 * 
	 * @param errCode
	 * @param errMsg
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:01:47
	 */
	protected Response getErrResponse(String errCode, String errMsg) {
		return Response.RESULT(errCode, errMsg);
	}

	/**
	 * 
	 * <p>获得业务处理响应数据</p>
	 * 
	 * @param principal
	 * @param request
	 * @return
	 * @author 苏镇威 2018年8月14日 上午10:01:57
	 */
	private Response getBizResponse(T request) {
		try {
			return bizProcess(request);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return getErrResponse(TransactionCode.E_9999, TransactionCode.E_9999_M);
		}
	}

	/**
	 * 
	 * <p>各个子业务处理方法</p>
	 * 
	 * @param principal
	 * @param request
	 * @return
	 * @throws Exception
	 * @author 苏镇威 2018年8月14日 上午10:02:10
	 */
	protected abstract Response bizProcess(T request) throws Exception;

}
