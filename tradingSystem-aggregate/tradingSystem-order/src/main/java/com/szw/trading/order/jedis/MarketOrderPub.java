package com.szw.trading.order.jedis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import redis.clients.jedis.JedisPool;


@Component
public class MarketOrderPub {
	protected Logger log = Logger.getLogger(getClass());
	@Autowired
	private JedisPool jedisPool;

	public void marketOrderPub(String channel, String message) {
		log.info("【市价单-发布者】channel：" + channel + "，发送参数：" + message);
		jedisPool.getResource().publish(channel, message);
	}
}
