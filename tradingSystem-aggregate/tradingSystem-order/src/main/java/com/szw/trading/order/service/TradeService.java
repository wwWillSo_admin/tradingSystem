package com.szw.trading.order.service;

import com.szw.trading.persistence.entity.Order;
import com.szw.trading.web.bean.Response;


public interface TradeService {
	public Response marketOrderTrade(Order order);
}
