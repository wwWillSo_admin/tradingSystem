package com.szw.trading.order.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


@Configuration
@PropertySource(value = { "classpath:application.properties", "classpath:persistence.properties", "classpath:redis.properties",
		"classpath:threadPool.properties" })
@EnableWebMvc
@ComponentScan(basePackages = { "com" })
@EnableJpaRepositories(basePackages = { "com" })
@EntityScan(basePackages = { "com" })
public class WebConfig implements WebMvcConfigurer {
	@Value("${spring.redis.host}")
	private String redisHost;
	@Value("${spring.redis.port}")
	private Integer redisPort;
	@Value("${spring.redis.password}")
	private String redisPass;
	@Value("${spring.redis.database}")
	private Integer redisDBNo;

	@Autowired
	private RedisTemplate redisTemplate;

	@Bean("redisTemplate")
	public RedisTemplate RedisTemplate() {

		RedisSerializer<String> redisSerializer = new StringRedisSerializer();// Long类型不可以会出现异常信息;
		redisTemplate.setKeySerializer(redisSerializer);
		Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
		ObjectMapper om = new ObjectMapper();
		om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		jackson2JsonRedisSerializer.setObjectMapper(om);

		redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
		redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
		redisTemplate.afterPropertiesSet();

		return redisTemplate;
	}

	@Bean("jedisPool")
	public JedisPool jedisPool() {
		return new JedisPool(new JedisPoolConfig(), redisHost, redisPort, 0, StringUtils.isNotBlank(redisPass) ? redisPass : null, redisDBNo);
	}

	// @Bean
	// public FilterRegistrationBean characterEncodingFilter() {
	// FilterRegistrationBean registration = new FilterRegistrationBean();
	// final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
	// characterEncodingFilter.setEncoding("UTF-8");
	// characterEncodingFilter.setForceEncoding(true);
	// ;
	// registration.setFilter(characterEncodingFilter);
	// registration.addUrlPatterns("/*");
	// return registration;
	// }

}
