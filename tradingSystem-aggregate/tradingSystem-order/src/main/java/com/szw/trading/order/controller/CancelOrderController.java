package com.szw.trading.order.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.order.service.OrderService;
import com.szw.trading.web.bean.CancelOrderRequest;
import com.szw.trading.web.bean.Response;
import com.szw.util.LogMapUtils;


@Controller
public class CancelOrderController extends CommonController<CancelOrderRequest> {

	@Autowired
	private OrderService orderService;

	@RequestMapping("/cancelOrder")
	@ResponseBody
	public Response cancelOrder(@Valid @RequestBody CancelOrderRequest request, BindingResult bindingResult) {
		log.info("[撤销订单] begin: the order = {}", LogMapUtils.toLogMap(request));
		// 获得业务处理响应结果
		Response response = super.getResponse(request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(CancelOrderRequest request) throws Exception {
		return orderService.cancelOrder(request);
	}

}
