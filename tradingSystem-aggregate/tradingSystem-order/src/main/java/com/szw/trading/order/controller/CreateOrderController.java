package com.szw.trading.order.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szw.trading.order.service.OrderService;
import com.szw.trading.web.bean.CreateOrderRequest;
import com.szw.trading.web.bean.Response;
import com.szw.util.LogMapUtils;


@Controller
public class CreateOrderController extends CommonController<CreateOrderRequest> {

	@Autowired
	private OrderService orderService;

	@RequestMapping("/createOrder")
	@ResponseBody
	public Response createOrder(@Valid @RequestBody CreateOrderRequest request, BindingResult bindingResult) {
		log.info("[创建订单] begin: the order = {}", LogMapUtils.toLogMap(request));
		// 获得业务处理响应结果
		Response response = super.getResponse(request, bindingResult);
		return response;
	}

	@Override
	protected Response bizProcess(CreateOrderRequest request) throws Exception {
		return orderService.createOrder(request);
	}

}
