package com.szw.trading.order.jedis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.szw.trading.order.service.TradeService;
import com.szw.trading.persistence.entity.Order;
import com.szw.trading.web.bean.Response;

import redis.clients.jedis.JedisPubSub;


/***
 * 市价单-订阅者
 * 
 * @author 苏镇威 2019年1月11日 下午4:11:16
 */
@Component
public class MarketOrderSub extends JedisPubSub {
	@Autowired
	private TradeService tradeService;
	protected Logger log = Logger.getLogger(getClass());

	@Override
	public void onMessage(String channel, String message) {    // 收到消息会调用
		log.info("【市价单-订阅者】接收参数：" + message);
		Order order = JSON.parseObject(message, Order.class);
		Response resp = tradeService.marketOrderTrade(order);
		log.info("【市价单-订阅者】处理结果：" + JSON.toJSONString(resp));
	}

	@Override
	public void onSubscribe(String channel, int subscribedChannels) {  // 订阅了频道会调用
		System.out.println(String.format("subscribe redis channel success, channel %s, subscribedChannels %d", channel, subscribedChannels));
	}

	@Override
	public void onUnsubscribe(String channel, int subscribedChannels) {  // 取消订阅 会调用
		System.out.println(String.format("unsubscribe redis channel, channel %s, subscribedChannels %d", channel, subscribedChannels));

	}
}
