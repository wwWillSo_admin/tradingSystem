package com.szw.trading.order.jedis;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;


/**
 * 添加订阅者
 * 
 * @author 苏镇威 2019年1月11日 下午4:13:53
 */
public class SubThread extends Thread {
	protected Logger log = Logger.getLogger(getClass());

	private final JedisPool jedisPool;
	private final MarketOrderSub subscriber;
	private final String channelName;

	public SubThread(JedisPool jedisPool, MarketOrderSub subscriber, String channelName) {
		super("MarketOrderSubThread");
		this.jedisPool = jedisPool;
		this.subscriber = subscriber;
		this.channelName = channelName;
	}

	@Override
	public void run() {
		log.info("【市价单交易请求】线程启动...");
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();  // 取出一个连接
			jedis.subscribe(subscriber, channelName);  // 通过subscribe 的api去订阅，入参是订阅者和频道名
		} catch (Exception e) {
			log.error("【添加订阅者】注册市价单订阅者失败...", e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}
}
