package com.szw.trading.order;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.szw.trading.order.jedis.MarketOrderSub;
import com.szw.trading.order.jedis.SubThread;
import com.szw.trading.order.processor.TradingSystemThreadPoolExecutor;
import com.szw.trading.order.runnable.LimitOrderQueueRunnable;
import com.szw.trading.persistence.entity.Order;
import com.szw.trading.web.constants.OrderQueueName;
import com.szw.util.RedisCacheUtil;

import redis.clients.jedis.JedisPool;


@SpringBootApplication
public class OrderApplication extends SpringBootServletInitializer {
	private final Logger log = Logger.getLogger(OrderApplication.class);

	@Autowired
	private RedisCacheUtil<Order> redisCacheUtil;
	@Value("${getMarketDataByCode.url}")
	private String getMarketDataByCodeUrl;
	@Value("${trade.url}")
	private String tradeUrl;
	@Value("${market.url}")
	private String marketUrl;
	private String title = "marketdata";

	@Autowired
	private JedisPool jedisPool;
	@Autowired
	private MarketOrderSub marketOrderSub;

	@Autowired
	private TradingSystemThreadPoolExecutor threadPoolExecutor;

	@PostConstruct
	public void init() {
		threadPoolExecutor.init();
		// 开启限价单处理任务
		threadPoolExecutor.addTask(new LimitOrderQueueRunnable(redisCacheUtil, marketUrl, title, tradeUrl));
		// 开启市价单处理任务
		new SubThread(jedisPool, marketOrderSub, OrderQueueName.MARKET_ORDER_QUEUE.name()).start();
		// threadPoolExecutor.addTask(new MarketOrderQueueRunnable(redisCacheUtil,
		// getMarketDataByCodeUrl, tradeUrl));
	}

	public static void main(String[] args) {

		SpringApplication.run(OrderApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OrderApplication.class);
	}
}
