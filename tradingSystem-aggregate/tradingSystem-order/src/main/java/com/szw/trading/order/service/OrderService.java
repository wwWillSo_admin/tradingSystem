package com.szw.trading.order.service;

import com.szw.trading.web.bean.CancelOrderRequest;
import com.szw.trading.web.bean.CreateOrderRequest;
import com.szw.trading.web.bean.Response;


public interface OrderService {
	Response createOrder(CreateOrderRequest request);

	Response cancelOrder(CancelOrderRequest request);
}
