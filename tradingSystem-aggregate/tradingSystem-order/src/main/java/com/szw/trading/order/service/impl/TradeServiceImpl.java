package com.szw.trading.order.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.szw.trading.order.service.BaseService;
import com.szw.trading.order.service.TradeService;
import com.szw.trading.persistence.entity.Order;
import com.szw.trading.persistence.entity.RealTimeMarketdata;
import com.szw.trading.web.bean.Response;
import com.szw.trading.web.constants.StatusCode;
import com.szw.util.HttpClientUtils;


/**
 * 交易接口集合
 * 
 * @author 苏镇威 2019年1月11日 下午4:12:45
 */
@Service
public class TradeServiceImpl extends BaseService implements TradeService {

	@Value("${getMarketDataByCode.url}")
	private String getMarketDataByCodeUrl;
	@Value("${trade.url}")
	private String tradeUrl;

	/**
	 * 处理市价单
	 * 
	 * @see com.szw.trading.order.service.TradeService#marketOrderTrade(com.szw.trading.persistence.entity.Order)
	 * @author 苏镇威 2019年1月11日 下午4:13:02
	 */
	@Override
	public Response marketOrderTrade(Order order) {
		try {
			String entity = HttpClientUtils.doGet(getMarketDataByCodeUrl + order.getStockCode());
			RealTimeMarketdata marketdata = JSON.parseObject(entity, RealTimeMarketdata.class);
			order.setOrderPrice(marketdata.getNow());

			BigDecimal cost = order.getOrderPrice().multiply(order.getOrderHand());
			BigDecimal serv = cost.multiply(BigDecimal.valueOf(0.001));

			order.setOrderAmount(cost);
			order.setServiceAmount(serv);

			// 调用trade接口
			String returnStr = HttpClientUtils.doPost(tradeUrl + "/trade", JSON.toJSONString(order));
			if (null != returnStr) {
				log.info("【市价单交易请求】returnStr：" + returnStr);

				try {
					Response resp = JSON.parseObject(returnStr, Response.class);
					if (!resp.get_ReturnCode().equals(StatusCode.SUCCESS.getCode())) {
						log.info("【市价单交易请求】交易失败，order：" + JSON.toJSONString(order));
					} else {
						log.info("【市价单交易请求】交易成功，order：" + JSON.toJSONString(order));
					}
					return resp;
				} catch (Exception e) {
					// log.info("【市价单交易请求】请求失败，重新进入市价单队列，order：" + JSON.toJSONString(order));
					// redisCacheUtil.pushCacheList(OrderQueueName.MARKET_ORDER_QUEUE.name(),
					// order);
					log.error("【市价单交易请求】出现异常...", e);
					return Response.FAILUE();
				}
			} else {
				// log.info("【市价单交易请求】请求失败，重新进入市价单队列，order：" + JSON.toJSONString(order));
				// redisCacheUtil.pushCacheList(OrderQueueName.MARKET_ORDER_QUEUE.name(), order);
				log.info("【市价单交易请求】请求失败，order：" + JSON.toJSONString(order));
				return Response.FAILUE();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			// log.info("【市价单交易请求】出现异常，重新进入市价单队列，order：" + JSON.toJSONString(order));
			// redisCacheUtil.pushCacheList(OrderQueueName.MARKET_ORDER_QUEUE.name(), order);
			log.error("【市价单交易请求】出现异常...", e);
			return Response.FAILUE();
		} finally {
		}
	}
}
